function counterFactory() {
    let counter=9;
        function increment()
        {
            return ++counter;
        }
        function decrement()
        {
            return --counter;
        }
        let obj={inc:increment,
                dec:decrement}
        return obj
    }

    export{
        counterFactory
    }