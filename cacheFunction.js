
function cacheFunction(cb) {
    const cache=new Object()
    function invoke(a)
    {
        if(typeof cache[a]!="undefined")
        {
            return cache[a]
        }
        else{
            cache[a]=cb(a)
            return cache[a]
        }
    }
    return {invoke:invoke}
}

export{
    cacheFunction
}