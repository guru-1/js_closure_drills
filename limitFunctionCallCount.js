function limitFunctionCallCount(cb, n) {
    let times=n
    function invoke()
    {
        if(times>0)
        {
            cb()
            times--
        }
        else{
            console.log("null")
        }
    }
    return {invoke:invoke}
}

export{
    limitFunctionCallCount
}